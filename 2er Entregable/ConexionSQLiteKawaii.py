import sqlite3
class PadreDinosaurio:
    def __init__(self):
        self.atributo1 = "padreDinosaurio"

    def metodo1(self):
        return "setter y getterd de padre"
class HijoDinosaurio:
    def __init__(self):
        self.atributo2 = " hijoDinosaurio "

    def metodo2(self):
        return "setter y getterd de hijp "

#######################################################################################################################
class ConexionSQLiteKawaii:
    def __init__(self, mi_base_de_datos):
        self.nombre_basededatos = mi_base_de_datos
        self.conexion = None

    def conectar(self):
        try:
            self.conexion = sqlite3.connect(self.nombre_basededatos)
            print(f"¡Conexión a la base de datos '{self.nombre_basededatos}' establecida! (｡♥‿♥｡)")
        except sqlite3.Error as error_conexion:
            print(f"Error al conectar a la base de datos: {error_conexion}")

    def desconectar(self):
        if self.conexion:
            self.conexion.close()
            print(f"Conexión a la base de datos '{self.nombre_basededatos}' cerrada ")
      
    def ejecutar_sql(self, consulta_sql, parametros=None):
        try:
            cursor = self.conexion.cursor()
            if parametros:
                cursor.execute(consulta_sql, parametros)
            else:
                cursor.execute(consulta_sql)
            self.conexion.commit()
            print("¡Consulta ejecutada con éxito! (ﾉ◕ヮ◕)ﾉ*:･ﾟ✧")
        except sqlite3.Error as error_ejecucion:
            print(f"Error {error_ejecucion}")
###########################################################################################################

if __name__ == "__main__":
    mi_base_datos = "mi_base_de_datos"
    conexion = ConexionSQLiteKawaii(mi_base_datos)
    
    conexion.conectar()

    # Acceder a métodos y atributos de papa dragon
    print(conexion.PadreDinosaurio.metodo1())
    print(conexion.PadreDinosaurio.atributo1)

    # Acceder a métodos y atributos de hijo dragon 
    print(conexion.HijoDinosaurio.metodo2())
    print(conexion.HijoDinosaurio.atributo2)
    
    crear_tabla_sql = """
     MiTabla (no funcional) (
     
    )
    """
    conexion.ejecutar_sql(crear_tabla_sql)
    
    insertar_datos_sql = " MiTabla (alimentacion, longitud)"
    parametros = (30)
    conexion.ejecutar_sql(insertar_datos_sql, parametros)

    consulta_datos_sql = "SELECT *  MiTabla"
    cursor = conexion.conexion.cursor()
    cursor.execute(consulta_datos_sql)
    resultados = cursor.fetchall()
    for fila in resultados:
        print(f"habitat: {fila[0]}, longitud: {fila[1]}, alimentacion: {fila[2]}")
   
    conexion.desconectar()
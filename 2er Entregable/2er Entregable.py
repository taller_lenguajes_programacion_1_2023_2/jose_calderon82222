import openpyxl


class PadreDinosaurio:
    def __init__(self, nombre=None, longitud=None, altura=None, peso=None, alimentacion=None, periodo=None):
        self.nombre = nombre
        self.longitud = longitud
        self.altura = altura
        self.peso = peso
        self.alimentacion = alimentacion
        self.periodo = periodo

    def get_nombre(self):
        return self.nombre

    def set_nombre(self, nombre):
        self.nombre = nombre

    def get_longitud(self):
        return self.longitud

    def set_longitud(self, longitud):
        self.longitud = longitud

    def get_altura(self):
        return self.altura

    def set_altura(self, altura):
        self.altura = altura

    def get_peso(self):
        return self.peso

    def set_peso(self, peso):
        self.peso = peso

    def get_alimentacion(self):
        return self.alimentacion

    def set_alimentacion(self, alimentacion):
        self.alimentacion = alimentacion

    def get_periodo(self):
        return self.periodo

    def set_periodo(self, periodo):
        self.periodo = periodo


class HijoDinosaurio(PadreDinosaurio):
    def __init__(self, nombre=None, longitud=None, altura=None, peso=None, alimentacion=None, periodo=None,
                 tipo=None, habitat=None, extincion=None, colores=None, comportamiento=None, especimen_unico=None):
        super().__init__(nombre, longitud, altura, peso, alimentacion, periodo)
        self.tipo = tipo
        self.habitat = habitat
        self.extincion = extincion
        self.colores = colores
        self.comportamiento = comportamiento
        self.especimen_unico = especimen_unico

    def get_tipo(self):
        return self.tipo

    def set_tipo(self, tipo):
        self.tipo = tipo

    def get_habitat(self):
        return self.habitat

    def set_habitat(self, habitat):
        self.habitat = habitat

    def get_extincion(self):
        return self.extincion

    def set_extincion(self, extincion):
        self.extincion = extincion

    def get_colores(self):
        return self.colores

    def set_colores(self, colores):
        self.colores = colores

    def get_comportamiento(self):
        return self.comportamiento

    def set_comportamiento(self, comportamiento):
        self.comportamiento = comportamiento

    def get_especimen_unico(self):
        return self.especimen_unico

    def set_especimen_unico(self, especimen_unico):
        self.especimen_unico = especimen_unico


######################################################################################3
workbook = openpyxl.Workbook()


sheet_papa = workbook.create_sheet("Papa Dinosaurio")
sheet_hijo = workbook.create_sheet("Hijo Dinosaurio")


atributos_papa = ["Nombre", "Longitud", "Altura", "Peso", "Alimentacion", "Período"]

sheet_papa.append(atributos_papa)


atributos_hijo = ["Nombre", "Longitud", "Altura", "Peso", "Alimentación", "Período",
                  "Tipo", "Hábitat", "Extinción", "Colores", "Comportamiento", "Espécimen unico"]
sheet_hijo.append(atributos_hijo)
workbook.save("Dinosaurios.xlsx")


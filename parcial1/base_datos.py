import sqlite3


conn = sqlite3.connect('consulta.db')
x = conn.cursor()

#Consulta
x.execute('''Consulta (
        consulta_id INTEGER PRIMARY KEY,
        fecha DATE,
        paciente TEXT,
        hospital hospital,
        diagnostico TEXT)''')

#Medica
x.execute('''Medica (
        medica_id INTEGER PRIMARY KEY,
        nombre TEXT,
        apellido TEXT,
        dni TEXT,
        especialidad TEXT,
        años_experiencia INTEGER)''')

#Hospital
x.execute('''  Hospital (
        hospital_id INTEGER PRIMARY KEY,
        especialidad TEXT,
        telefono TEXT,
        direccion TEXT,
        nombre TEXT,
        capacidad INTEGER)''')

#Paciente
x.execute(''' Paciente (
        paciente_id INTEGER PRIMARY KEY,
        nombre TEXT,
        apellido TEXT,
        dni TEXT,
        nacimiento DATE,
        tipo_sangre TEXT)''')

#dios santo, ayudame D:
conn.commit()
conn.close()






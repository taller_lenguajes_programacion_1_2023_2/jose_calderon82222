import sqlite3
import json 
class DatabaseConnection:
    def __init__(self, db_name):
        self.db_name = db_name
        self.conn = None

    def connect(self):
        self.conn = sqlite3.connect(self.db_name)

    def close(self):
        if self.conn:
            self.conn.close()

    def execute_query(self, query):
        cursor = self.conn.cursor()
        cursor.execute(query)
        self.conn.commit()
        cursor.close()

    def select(self, query):
        cursor = self.conn.cursor()
        cursor.execute(query)
        result = cursor.fetchall()
        cursor.close()
        return result

    def insert(self, query):
        self.execute_query(query)

    def delete(self, query):
        self.execute_query(query)
##################################################################3
#parte json
    def execute_queries_from_json(self, json_file):
        with open(json_file, 'r') as file:
            queries = json.load(file)
            for query in queries:
                self.execute_query(query)
                
###################################################################################################333333
#ff
from consulta import Consulta
class Medica(Consulta):
    def __init__(self, nombre, apellido, dni, especialidad, años_experiencia, fecha, paciente, hospital, diagnostico):
        super().__init__(fecha, paciente, hospital, diagnostico)
        self.nombre = nombre
        self.apellido = apellido
        self.dni = dni
        self.especialidad = especialidad
        self.años_experiencia = años_experiencia
        self.medica_id = id(self)


        nombre_datos = "Juan"
        apellido_datos = "López"
        dni_datos = "123456789"
        especialidad_datos = "Medicina General"
        años_experiencia_datos = 5

        medica = Medica(nombre_datos, apellido_datos, dni_datos, especialidad_datos, años_experiencia_datos)

        print("ID de Medica:", medica.clase_id)

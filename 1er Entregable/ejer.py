#1
print("hola")
#2
nombre = "Juan"
edad = 25

#3
numero = 42
decimalSimple = 3.14
decimalDoble = 3.14159265359
mensaje = "Hola, mundo"

#4
num1 = 5
num2 = 3
resultado = num1 + num2
print("La suma es:", resultado)

#5

a = 5  
b = 2.0
c = a + b

num = 42
num_str = str(num)

#6
nombre = input("Por favor, ingresa tu nombre: ")
print("Hola, " + nombre + "! Bienvenido.")

#7 8 9
edad = 25

if edad < 18:
    print("Eres menor de edad.")
elif edad >= 18 and edad < 65:
    print("Eres adulto.")
else:
    print("Eres un adulto mayor.")

#10
for i in range(1, 6):
    print(i)
    
frutas = ["manzana", "banana", "cereza"]
for fruta in frutas:
    print(fruta)
    
#11

contador = 1
while contador <= 5:
    print(contador)
    contador += 1
    
#12

for i in range(1, 11):
    if i == 5:
        break  # Salir del bucle cuando i sea igual a 5
    print(i)
    
for i in range(1, 11):
    if i == 5:
        continue  # Omitir la iteración cuando i sea igual a 5
    print(i)

#13
mi_lista = [1, 2, 3, 4, 5]
otra_lista = ["manzana", "banana", "cereza"]
mezcla = [1, "dos", True, 3.14]
primer_elemento = mi_lista[0]
ultimo_elemento = mi_lista[-1]

#14 

mi_tupla = (1, 2, 3, "hola")
mi_tupla[0] = 10  # Esto generará un error

#15

conjunto={"a","b","c"}
conjunto.add(5)

#16
mi_diccionario = {"nombre": "Juan", "edad": 30, "ciudad": "Ejemplo"}

#17

mi_lista = [1, 2, 3]
mi_lista.append(4)

#18

with open("mi_archivo.txt", "r") as archivo:
    contenido = archivo.read()
print(contenido)

#19

with open("mi_archivo.txt", "w") as archivo:
    
    archivo.write("Hola, esto es una línea de texto.\n")
    archivo.write("Esta es otra línea de texto.\n")
    
#20

print("r:leer, w:escribir, a:añador, rb:leer imagen, wb:escribir imagen")

#21

import json
datos = {
    "nombre": "Juan",
    "edad": 30,
    "ciudad": "Ejemplo"
}
with open("datos.json", "w") as archivo:
    json.dump(datos, archivo)

import pandas as pd

df = pd.read_json("datos.json")

print("Contenido del DataFrame:")
print(df)

#22

import pandas as pd
df = pd.read_csv("Hoja de cálculo sin título - Hoja 1.csv")
    
#23

import pandas as pd

datos = {
    "Nombre": ["Juan", "Ana", "Carlos", "Luis"],
    "Edad": [30, 25, 35, 28],
    "Ciudad": ["Ejemplo", "OtroEjemplo", "OtroEjemplo", "Ejemplo"]
}

df = pd.DataFrame(datos)

mayores_de_30 = df.query("Edad > 30")
print(mayores_de_30)
print("paco")
#sesion2"""
#1
import pandas as pd
url = "https://www.um.es/documents/877924/4630870/Mayores2018+Mat+Apli+CCSS+-+tabla_de_la_distribucion_normal3-1.pdf/fdcdf99d-b6d6-49c8-82af-eded690dbf4f"
tablas = pd.read_html(url)
tabla = tablas[0]
print(tabla)

#2

import pandas as pd
url = "https://www.um.es/documents/877924/4630870/Mayores2018+Mat+Apli+CCSS+-+tabla_de_la_distribucion_normal3-1.pdf/fdcdf99d-b6d6-49c8-82af-eded690dbf4f"
tablas = pd.read_html(url)
tabla = tablas[0]
nombre_archivo_excel = "datos.xlsx"
tabla.to_excel(nombre_archivo_excel)

#3

tabla.head(10).to_csv("C:\Users\pc\Desktop\taller1\Hoja de cálculo sin título - Hoja 1")

#4

import pandas as pd
#print(hola) 
# Datos para el primer DataFrame
datos1 = {
    "Nombre": ["Juan", "Ana", "Carlos"],
    "Edad": [30, 25, 35],
    "Ciudad": ["Ejemplo", "OtroEjemplo", "OtroEjemplo"]
}

# Datos para el segundo DataFrame
datos2 = {
    "Producto": ["A", "B", "C"],
    "Precio": [10.5, 15.2, 8.0],
    "Stock": [100, 50, 75]
}

# Datos para el tercer DataFrame
datos3 = {
    "País": ["México", "Argentina", "España"],
    "Población": [126, 45, 47],
    "Capital": ["Ciudad de México", "Buenos Aires", "Madrid"]
}

df1 = pd.DataFrame(datos1)
df2 = pd.DataFrame(datos2)
df3 = pd.DataFrame(datos3)

with pd.ExcelWriter("tres_tablas.xlsx", engine="xlsxwriter") as writer:

    df1.to_excel(writer, sheet_name="Hoja1", index=False)
    df2.to_excel(writer, sheet_name="Hoja2", index=False)
    df3.to_excel(writer, sheet_name="Hoja3", index=False)
    
#5
Doc = pd.read_excel(".C:\Users\pc\Desktop\taller1.xlsx")
filtroEdad = Doc.loc[Doc["Edad"] >= 30]
filtroEdad.to_csv(".C:\Users\pc\Desktop\taller1.csv")

#6





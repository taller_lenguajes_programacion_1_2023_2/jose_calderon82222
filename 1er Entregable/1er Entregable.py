#1) un programa que determine si una variable n es par o impar 

JMC_n = int(input("Ingrese un número entero: "))
if JMC_n % 2 == 0:
    print("es un número par.")
else:
    print("es un número impar.")

#2) escribe un programa que determine si una cadena es un palindromo o no 


JMC_cadena = input("Ingrese una cadena: ")

JMC_cadena = JMC_cadena.replace(" ", "").lower()

if JMC_cadena == JMC_cadena[::-1]:
    
    print(" es un palíndromo.")
    
else:
    
    print(" no es un palíndromo.")
    
#3) un programa que sume los elemntos de una lista 

JMC_lista = [1, 2, 3, 4, 5]

JMC_suma = 0

for JMC_elementos in JMC_lista:
    
    JMC_suma += JMC_elementos
    
    print(JMC_suma)
    
#4) escribe un progrma que combine dos diccionarios en uno 

JMC_diccionario1 = {"nombre": "Juan", "edad": 30, "ciudad": "rionegro"}
JMC_diccionario2 ={"nombre": "pepe", "edad": 25, "ciudad": "marinilla"}
JMC_diccionario3= {}
JMC_diccionario3.update(JMC_diccionario1)
JMC_diccionario3.update(JMC_diccionario2)

print(JMC_diccionario3)

#5)un programa que agruoe los datos por una columna especifica y calcule la suma de otra columan para cada grupo 
import pandas as pd 

JMC_data = pd.read_csv("datos.csv")
JMC_resultados = JMC_data.groupby("ciudad")["edad"].sum()
print(JMC_resultados)

#6)escribe un pragrama que filtre y muestre solo aquellos empleados cuyo salario sea mayor a 3000

import pandas as pd


archivo_excel = 'dato_excel.xlsx' 
df = pd.read_excel(archivo_excel)

# Filtrar empleados con salario > 3000
JMC_empleados_filtrados = df[df['salario'] > 3000]


print(JMC_empleados_filtrados)


import pandas as pd


archivo_excel = 'dato_excel.xlsx' 
df = pd.read_excel(archivo_excel)

JMC_nombre_columna_original = 'empleados ' 
JMC_nombre_columna_nuevo = 'new_empleados '  

if JMC_nombre_columna_original in df.columns:
    df.rename(columns={JMC_nombre_columna_original: JMC_nombre_columna_nuevo}, inplace=True)
else:
    print(f"La columna '{JMC_nombre_columna_original}' no existe en el DataFrame.")
#8)


